# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="zelims"

# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  git
)

source $ZSH/oh-my-zsh.sh

bindkey "^[[1~" beginning-of-line
bindkey "^[[4~" end-of-line

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

for file in ~/.{path,aliases,functions}; do
        if [[ -r "$file" ]] && [[ -f "$file" ]]; then
                # shellcheck source=/dev/null
                source "$file"
        fi
done
unset file

export GOPATH=$HOME/Programming/Go
export PATH=$PATH:$HOME/.bin:$GOPATH/bin
