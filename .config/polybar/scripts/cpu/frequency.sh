
#!/bin/sh

GOV=$(cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor)

notify() {
	notify-send "CPU State" "${1}" -i processor
}

toggle_governor() {
	if [[ $GOV == "powersave" ]]; then
		notify "Changed to performance"
		$HOME/.bin/cpugov performance
	elif [[ $GOV == "performance" ]]; then
		notify "Changed to powersave"
		$HOME/.bin/cpugov powersave
	else
		notify "ERROR: Could not find CPU Governor status"
	fi
}

print_cpu() {
	c=0;t=0
	awk '/MHz/ {print $4}' < /proc/cpuinfo | (while read -r i
	do
	    t=$( echo "$t + $i" | bc )
	    c=$((c+1))
	done
	PRINT_GOV="E"
	if [[ $GOV == "powersave" ]]; then
		PRINT_GOV="S"
	elif [[ $GOV == "performance" ]]; then
		PRINT_GOV="P"
	fi
	echo $(echo "scale=2; $t / $c / 1000" | bc | awk '{print $1" GHz"}') "(${PRINT_GOV})")
}

case "$1" in
	--toggle)
		toggle_governor
		;;
	*)
		print_cpu
		;;
esac
