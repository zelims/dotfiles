#!/bin/sh
#
# Author: zelims
# Web: zelims.me
# 

get_artist() {
	echo $(echo "$1" | awk '
	/string  *"xesam:artist/{
		while (1) {
			getline line
			if (line ~ /string "/){
				sub(/.*string "/, "", line)
				sub(/".*$/, "", line)
				print line
				break
			}
		}
	}')
}

get_title() {
	echo $(echo "$1" | awk '
	/string  *"xesam:title/{
		while (1) {
	  		getline line
	  		if (line ~ /string "/){
	    		sub(/.*string "/, "", line)
	    		sub(/".*$/, "", line)
	    		print line
	    		break
	  		}
		}
	}
	')
}

get_album() {
	echo $(echo "$1" | awk '
	/string  *"xesam:album"/{
		while (1) {
	  		getline line
	  		if (line ~ /string "/){
	    		sub(/.*string "/, "", line)
	    		sub(/".*$/, "", line)
	    		print line
	    		break
	  		}
		}
	}
	')
}

if [[ $(pidof spotify) ]]; then
	status=$(dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.freedesktop.DBus.Properties.Get string:'org.mpris.MediaPlayer2.Player' string:'PlaybackStatus'|egrep -A 1 "string"|cut -b 26-|cut -d '"' -f 1|egrep -v ^$)
else
	exit
fi

case $status in
	"Playing")
	play_pause="契"
	;;

	"Paused")
	play_pause=""
	;;

	*)
	play_pause=""
	exit
	;;
esac

metadata=$(dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.freedesktop.DBus.Properties.Get string:'org.mpris.MediaPlayer2.Player' string:'Metadata')

title=$(get_title "$metadata")
artist=$(get_artist "$metadata")
album=$(get_album "$metadata")

if [[ ! -z "$title" ]] || [[ ! -z "$artist" ]] || [[ ! -z "$album" ]]; then
	echo "${play_pause}  ${title} - ${artist}"
fi