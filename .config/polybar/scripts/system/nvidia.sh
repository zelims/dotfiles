#!/bin/sh

echo $(nvidia-smi --query-gpu=utilization.gpu,utilization.memory,temperature.gpu,clocks.gr --format=csv,noheader,nounits | awk -F ',' '{ printf ("%d%s\r%d%s\r%d%s", $1, "%", $3, "°C", $4, " MHz") }')