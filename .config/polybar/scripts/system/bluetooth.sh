#!/bin/sh

bluetooth_print() {
    if [ "$(systemctl is-active "bluetooth.service")" = "active" ] && [ "$(bluetoothctl show | grep "Powered: yes")" ]; then
        echo '%{F#EAEAEA}'
    else
        echo "%{F#9C9C9C}"
    fi
}

bluetooth_toggle() {
    if bluetoothctl show | grep -q "Powered: no"; then
        bluetoothctl power on >> /dev/null
        notify-send "Bluetooth" "Powered on" -i bluetooth-active -t 2000
        sleep 1
        devices_paired=$(bluetoothctl paired-devices | grep Device | cut -d ' ' -f 2)
        echo "$devices_paired" | while read -r line; do
            bluetoothctl connect "$line" >> /dev/null
        done
    else
        devices_paired=$(bluetoothctl paired-devices | grep Device | cut -d ' ' -f 2)
        echo "$devices_paired" | while read -r line; do
            bluetoothctl disconnect "$line" >> /dev/null
        done

        bluetoothctl power off >> /dev/null
        notify-send "Bluetooth" "Powered off" -i bluetooth-disabled -t 2000
    fi
}

case "$1" in
    --toggle)
        bluetooth_toggle
        ;;
    *)
        bluetooth_print
        ;;
esac
