#!/bin/sh
#
# Author: zelims
# Web: zelims.me
#

list_connections() {
	if [[ $(count_connections) -gt 0 ]]; then
		ips=$(get_connections | tr -s ' ' | cut -d' ' -f6 | cut -d':' -f1)
		notify-send "SSH Connections" "<b>Currently connected to:</b>\n$(echo $ips | tr " " "\n")" -i server-database
	fi
}

count_connections() {
	get_connections | wc -l
}

get_connections() {
	ss -an | grep 'ESTAB.*:22'
}

print_status() {
	count=$(count_connections)
	if [[ $count -gt 0 ]]; then
		echo "歷 ${count}"
	else
		echo "轢"
	fi
}

case "$1" in
	-l)
		list_connections
		;;
	*)
		print_status
esac