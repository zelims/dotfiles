#!/bin/sh

sinks=($(pacmd list-sinks | grep index | awk '{ if ($1 == "*") print "1"; else print "0" }'))

get_active_sink() {
    active=0
    for i in ${sinks[*]}
    do
        if [ $i -eq 0 ]
            then active=$((active+1))
            else break
        fi
    done
    echo $active
}

change_sink() {
    active=$(get_active_sink)
    swap=$(((active+1)%${#sinks[@]}))

    desc=$(pacmd list-sinks | grep "device.description" | head -n$(( $swap + 1 )) | tail -n1 | cut -d '"' -f2)
    notify-send "Changed Audio Device" "$desc" -i audio-player

    pacmd set-default-sink $swap &> /dev/null
    move_inputs $swap
}

move_inputs() {
    inputs=($(pacmd list-sink-inputs | grep index | awk '{print $2}'))
    for i in ${inputs[*]}; do pacmd move-sink-input $i $1 &> /dev/null; done
}

case "$1" in
    toggle) change_sink ;;
    move) move_inputs $(get_active_sink) ;;
    *) ;;
esac
