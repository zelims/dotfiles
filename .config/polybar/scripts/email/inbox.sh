#!/bin/sh

SERVER="imap.gmail.com"
NETRC="$HOME/.keys/.netrc"
FILE=".uid"

get_unread() {
	echo $(curl -sf --netrc-file "$NETRC" -X "STATUS INBOX (UNSEEN)" imaps://"$SERVER"/INBOX | tr -d -c "[:digit:]")
}

get_recent_uid() {
	echo $(curl -sf --netrc-file "$NETRC" -X "FETCH * (UID)" imaps://"$SERVER"/INBOX | grep -oP '\d*' | tail -1)
}

update_uid() {
	echo $1 > $FILE
}

unread=$(get_unread)

if [[ "$unread" -gt "0" ]]; then
	echo " $unread"
	if [[ ! -f "$FILE" ]]; then
		touch $FILE
	fi
	read NOTIFYID < $FILE
	uid=$(get_recent_uid)
	if [[ ! $uid -eq $NOTIFYID ]]; then
		name=$(awk -v s="$SERVER" '$0~s{getline; print $2}' $NETRC)
		notify-send "New Email" "Mailbox: ${name}" -i email
		update_uid $uid
	fi
else
	echo ""
fi
